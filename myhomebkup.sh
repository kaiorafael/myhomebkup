#!/usr/bin/env bash
# Copyright Kaio Rafael de Souza Barbosa
# @kaiux

#########  SETUP  ######################

# change here the backup destination
DEFAULT_BKP_DIR="/home/backupuser/"
#DEFAULT_BKP_DIR="/tmp"

## Compression parameter
TAR_CMD=`which tar`
TAR_ARGS="-P -zcf"
GZIP_CMD=`which gzip`

## verbose flag
VERBOSE_FLAG=0
TODAY_DATE=`date "+%F_%H-%M-%S"`

RUNNING_USER_PATH=$HOME

usage()
{
	echo "$0 [options]"
	echo "\t-d to override default backup destination"
	echo "\t-D to enable verbose"
}

check_perm()
{
	if [ ! -d ${DEFAULT_BKP_DIR} ]
	then
		echo "Sending email error no dir.. :("
		exit 1
	fi

	if [ ! -d ${RUNNING_USER_PATH} ]
	then
		echo "There is no ${RUNNING_USER_PATH}"
		exit 1
	fi

	## fix when USER is not defined
	if [ -z ${USER} ]
	then
		USER=$LOGNAME
	fi
}

###########################################
# Creates a tar file based and store at
# ${DEFAULT_BKP_DIR}
create_tar()
{

TARGZ_NAME="${DEFAULT_BKP_DIR}/${USER}_${TODAY_DATE}.tgz"

${TAR_CMD} ${TAR_ARGS} ${TARGZ_NAME} ${RUNNING_USER_PATH}
RESULT=$?

if [ ${RESULT} -eq 0 ]
then
	echo "Sending email ok"
else
	echo "Sending email error.. :("
fi

}

######################
#### main code
######################
check_perm
create_tar
